#!/bin/sh

# Sudoers
#--------

/usr/sbin/addsudo /usr/sbin/slapadd app-openldap
/usr/sbin/addsudo /usr/sbin/slapcat app-openldap

# Set default sysconfig
#----------------------

# ClearOS 6 only
if [ -e /etc/sysconfig/slapd ]; then
    CHECK=`grep ^BIND_POLICY /etc/sysconfig/slapd 2>/dev/null`

    if [ -z "$CHECK" ]; then
        logger -p local6.notice -t installer "app-openldap - updating LDAP sysconfig"
        cp /usr/clearos/apps/openldap/deploy/ldap.sysconfig /etc/sysconfig/slapd
    fi
fi

# Grab bootstrap certificates from Certificate Manager
#-----------------------------------------------------

/usr/sbin/system-certs \
    --app "openldap" \
    --description "Directory Server" \
    --key-file "/etc/openldap/cacerts/key.pem" \
    --key-owner "root" \
    --key-group "ldap" \
    --key-perms "0640" \
    --cert-file "/etc/openldap/cacerts/cert.pem" \
    --cert-owner "root" \
    --cert-group "ldap" \
    --cert-perms "0640"

# Fix slapd.d issue
#------------------

if ( [ -d /etc/openldap/slapd.d ] && [ -e /etc/openldap/slapd.conf.bak ] ); then
    logger -p local6.notice -t installer "app-openldap - fixing slapd.d issue"
    mv /etc/openldap/slapd.d /var/clearos/openldap/backup
    cp -a /etc/openldap/slapd.conf.bak /etc/openldap/slapd.conf
    service slapd restart >/dev/null 2>&1
fi

# Add new schemas
#----------------

DO_RESTART=""

# config.php check: don't bother with upgrade if LDAP not yet provisioned
if [ -e /var/clearos/openldap/config.php ]; then
    CHECK=`grep ^include[[:space:]]*/etc/openldap/schema/owncloud.schema /etc/openldap/slapd.conf 2>/dev/null`

    if [ -z "$CHECK" ]; then
        logger -p local6.notice -t installer "app-openldap - adding owncloud schema"
        # Messy - using samba3 as a file marker
        sed -i -e "s/^include[[:space:]]*\/etc\/openldap\/schema\/samba3.schema/include \/etc\/openldap\/schema\/samba3.schema\n\n# OwnCloud\ninclude \/etc\/openldap\/schema\/owncloud.schema/" /etc/openldap/slapd.conf
        DO_RESTART="yes"
    fi

    CHECK=`grep ^include[[:space:]]*/etc/openldap/schema/kopano /etc/openldap/slapd.conf 2>/dev/null`

    if [ -z "$CHECK" ]; then
        logger -p local6.notice -t installer "app-openldap - adding Kopano schema"
        # Messy - using samba3 as a file marker
        sed -i -e "s/^include[[:space:]]*\/etc\/openldap\/schema\/samba3.schema/include \/etc\/openldap\/schema\/samba3.schema\n\n# Kopano\ninclude \/etc\/openldap\/schema\/kopano.schema/" /etc/openldap/slapd.conf
        DO_RESTART="yes"
    fi

    # Nextcloud schema change, need to restart LDAP
    if [ ! -e /var/clearos/openldap/nextcloud-schema ]; then
        touch /var/clearos/openldap/nextcloud-schema
        DO_RESTART="yes"
    fi
fi

# Add empty netgroup file
#------------------------

if [ ! -e /etc/netgroup ]; then
    logger -p local6.notice -t installer "app-openldap - adding netgroup file"
    touch /etc/netgroup
fi

# Configure server certificates
#------------------------------

/var/clearos/events/certificate_manager/openldap

# Detect lingering file permission problem in /var/lib/ldap
#----------------------------------------------------------

if [ -e /var/lib/ldap/__db.001 ]; then
    OWNER=`stat --format=%U /var/lib/ldap/__db.001`

    if [ "$OWNER" == "root" ]; then
        logger -p local6.notice -t installer "app-openldap - fixing file permissions"
        chown -R ldap.ldap /var/lib/ldap
        service slapd restart
        service nslcd restart
        service nscd restart
    fi
fi

# Change log level
#-----------------

LOGLEVEL_CHECK=`grep "^loglevel[[:space:]]*256$" /etc/openldap/slapd.conf`
if ( [ -n "$LOGLEVEL_CHECK" ] || [ ! -e /var/clearos/openldap/config.php ] ); then
    logger -p local6.notice -t installer "app-openldap - updating log level"
    sed -i -e 's/^loglevel[[:space:]]*256$/loglevel 0/' /etc/openldap/slapd.conf
    DO_RESTART="yes"
    /sbin/service rsyslog condrestart >/dev/null 2>&1
fi

# Restart LDAP in case of schema changes
#---------------------------------------

if [ -n "$DO_RESTART" ]; then
    /sbin/service slapd condrestart >/dev/null 2>&1
fi
